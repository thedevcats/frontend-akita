import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CoreQuery } from './core/stores/core/core.query';
import { CoreService } from './core/stores/core/core.service';
import { Champion } from './models/champion';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  champions$: Observable<Array<Champion>> = this.coreQuery.champions$;
  selectedChampion$: Observable<Champion> = this.coreQuery.selectedChampion$;

  constructor(private coreQuery: CoreQuery, private coreService: CoreService) {}

  navigateToDetails(id: string) {
    this.coreService.getChampionById(id);
  }
}
