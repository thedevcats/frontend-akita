import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionBasketComponent } from './champion-basket.component';

describe('ChampionBasketComponent', () => {
  let component: ChampionBasketComponent;
  let fixture: ComponentFixture<ChampionBasketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChampionBasketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionBasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
