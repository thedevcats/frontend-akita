import { Component, OnInit } from '@angular/core';
import { Champion } from 'src/app/models/champion';

@Component({
  selector: 'app-champion-basket',
  templateUrl: './champion-basket.component.html',
  styleUrls: ['./champion-basket.component.scss']
})
export class ChampionBasketComponent implements OnInit {
  champions: Array<Champion> = [];

  constructor() {}

  ngOnInit() {}
}
