import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { CoreQuery } from '../../stores/core/core.query';
import { CoreService } from '../../stores/core/core.service';
import { Champion } from 'src/app/models/champion';

@Injectable({
  providedIn: 'root'
})
export class DetailsResolver implements Resolve<any> {
  selectedChampion: Champion;
  constructor(private coreQuery: CoreQuery, private coreService: CoreService) {
    this.coreQuery.selectedChampion$.subscribe(d => (this.selectedChampion = d));
  }

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.id;

    if (!this.selectedChampion || this.selectedChampion.id !== id) {
      this.coreService.getChampionById(id);
    }
  }
}
