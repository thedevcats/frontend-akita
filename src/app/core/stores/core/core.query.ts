import { Query } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { CoreState, CoreStore } from './core.store';
import { Champion } from 'src/app/models/champion';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CoreQuery extends Query<CoreState> {
  champions$: Observable<Array<Champion>> = this.select(state => state.champions);
  selectedChampion$: Observable<Champion> = this.select(state => state.selectedChampion);

  constructor(protected store: CoreStore) {
    super(store);
  }
}
