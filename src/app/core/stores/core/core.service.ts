import { Injectable } from '@angular/core';
import { CoreStore } from './core.store';
import { ApiService } from '../../services/api/api.service';
import { Champion } from 'src/app/models/champion';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class CoreService {
  constructor(
    private coreStore: CoreStore,
    private apiService: ApiService,
    private router: Router
  ) {
    this.getAllChampions();
  }

  getAllChampions() {
    this.apiService.getAllChampions().subscribe((champions: Array<Champion>) => {
      this.coreStore.update({
        champions
      });
    });
  }

  getChampionById(id: string) {
    this.apiService.getChampionById(id).subscribe((selectedChampion: Champion) => {
      this.coreStore.update({
        selectedChampion
      });
      this.router.navigate(['champions', id]);
    });
  }
}
