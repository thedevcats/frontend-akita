import { Injectable } from '@angular/core';
import { StoreConfig, Store } from '@datorama/akita';
import { Champion } from 'src/app/models/champion';

export interface CoreState {
  champions: Array<Champion>;
  selectedChampion: Champion;
}

export function createInitialState(): CoreState {
  return {
    champions: [],
    selectedChampion: null
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'core', resettable: true })
export class CoreStore extends Store<CoreState> {
  constructor() {
    super(createInitialState());
  }
}
